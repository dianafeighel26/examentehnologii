var person = {}; //<=> new Object();  //object literals
person.name = 'John';
person.surname='Doe';
console.log(person);

var person1 = {};
person1.nume = 'Jane';
person1.prenume = 'Doe';
console.log(person1);

//Js are constructori, dar nu clase =>constructor function

function Person(name,surname){
    //cand apelez this dintr o functie, this e referinta
    this.name=name;
    this.surname=surname;
}

//pt a adauga metode folosim prototype
Person.prototype.getDetailsNormal = function(){
    return this.name + ' ' + this.surname;
}

//din vers 6  ----> am `` in loc de "" si ''
Person.prototype.getDetailsInterpolation = function(){
    return `${this.name} ${this.surname}`;
}

//initializare obiect de tip Person
var person2 = new Person('Gigi','Popescu');
console.log(person2);

console.log(person2.getDetailsNormal());
console.log(person2.getDetailsInterpolation());

//Mostenire-metoda veche
function Student(name,surname,group){
    //apelare constructor din clasa de baza
    Person.call(this,name,surname);//accepta ca prim argument this
    this.group=group;
}

// //se face imediat dupa declarare
// Student.prototype = Object.create(Person.prototype);

Object.defineProperty(Student.prototype,'constructor',{
    value: Student, enumerable:false, writable:true });
 //1=ob pe care vreau sa il modific,2=propr pe care o modific, 3=
console.log(Student.prototype.constructor);
//Tema-metoda noua!!!!



Student.prototype.studentFactory = function(university){
    function signIn(){
        console.log(this);
        console.log(`${this.name} ${this.surname} signed in at the university: 
        ${university}`);
    }
    function graduate(){
        console.log(this);
        console.log(`${this.name} ${this.surname} graduated the university : ${university}. Hooray!`);
    }
    return {
        signIn: signIn,
        graduate: graduate
    };
}

Student.prototype.studentFactory1 = function(university){
    var self = this;
    function signIn(){
        console.log(self);
        console.log(`${self.name} ${self.surname} signed in at the university: 
        ${university}`);
    }
    function graduate(){
        console.log(self);
        console.log(`${self.name} ${self.surname} graduated the university : ${university}. Hooray!`);
    }
    return {
        signIn: signIn,
        graduate: graduate
    };
}

var student = new Student('Jane','Doe',1077);
console.log(student);
student.studentFactory1('ASE');
student.studentFactory1('ASE').signIn();
student.studentFactory1('ASE').graduate();




Student.prototype.studentFactory2 = function(university){
    function signIn(){
        console.log(this);
        console.log(`${this.name} ${this.surname} signed in at the university: 
        ${university}`);
    }
    function graduate(){
        console.log(this);
        console.log(`${this.name} ${this.surname} graduated the university : ${university}. Hooray!`);
    }
    return {
        signIn: signIn.bind(this),  //se face binding cu this ul din person
        graduate: graduate.bind(this)
    };
}

student.studentFactory2('POLI');
student.studentFactory2('POLI').signIn();
student.studentFactory2('POLI').graduate();

//arrow functions -> 6
//am garantia ca this ul folosit in arrow functions va face bindding catre contextul imediat superior
Student.prototype.studentFactory3 = function(university){
    signIn = () => {
        console.log(this);
        console.log(`${this.name} ${this.surname} signed in at the university: 
        ${university}`);
    }
    graduate= () =>{
        console.log(this);
        console.log(`${this.name} ${this.surname} graduated the university : ${university}. Hooray!`);
    }
    return {
        signIn: signIn,
        graduate: graduate
    };
}

student.studentFactory3('Universitate').signIn();
student.studentFactory3('Universitate').graduate();




























