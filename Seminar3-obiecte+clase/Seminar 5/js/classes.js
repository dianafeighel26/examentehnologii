//Metoda noua-> cu class

class Person{
    constructor(name,surname){
        this._name=name;
        this._surname=surname;
    }

    set name(value){
        this._name=value;
    }
    get name(){
        return this._name;
    }
    set surname(value){
        this._surname=value;
    }
    get surname(){
        return this._surname;
    }
}

//Mostenire
class Employee extends Person{
    constructor(name,surname,salary){
        super(name,surname);
        this.salary=salary;
    }
}





//abstractizare
//_ in fata numelui variabilei => private
//conventia cu _ e sa ma faca pe mine sa evit stackoverflow
var person = new Person('John','Doe');
console.log(person);
person.name= 'Jane';
console.log(person);


var emp1 = new Employee('E1','E1',5000);
var emp2 = new Employee('E2','E2',3000);
var emp3= new Employee('E3','E3',7000);

var employees = [];
employees.push(emp1);
employees.push(emp2);
employees.push(emp3);

console.log(employees.map(employee => employee.salary).reduce((total,salary) => total += salary , 0));
 //am mapat  vectorul meu initial la un alt vector ce contine doar salariile

//map,filter,reduce 







