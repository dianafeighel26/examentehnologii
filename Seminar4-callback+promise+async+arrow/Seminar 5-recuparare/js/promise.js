//nu mai am nevoie de callback
function authenticate(username,password){
    return new Promise((resolve,reject)=> {
        const payload = {
            data: {},
            error:null
        };
        setTimeout(() => {
            if(username ==='admin' && password === 'pass'){
                payload.data.authentication_token= 'SECRET';
                payload.data.expire_time = 5000;
                payload.data.role = 'ADMIN';
                resolve(payload.data);
            }else {
                payload.error = 'Invalid credentials';
                reject(payload.error);
            }
        },2000);
    });
}


function redirect(userData,path){
    return new Promise((resolve,reject) => {
        const payload={
            data:{},
            error:null
        };
        if(!userData.role){
            payload.error = 'Unauthorized';
            reject(payload.error);
        } else {
            if(path.includes('admin')){
                payload.data='Hello master';
            } else {
                payload.data='Welcome user';
            }
            resolve(payload.data);
        }
    });
}

authenticate('admin','pass').then(userData => {
    redirect(userData,'/home').then(msg => {
        alert(msg);
    }).catch(err => {throw err;})
}).catch(err => {
    throw err;
});

//async -> se realiz cu ajutorul generatorilor care ma ajuta sa stopez procesul de executie al unei functii

