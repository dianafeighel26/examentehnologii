//async -> se realiz cu ajutorul generatorilor care ma ajuta
//sa stopez procesul de executie al unei functii
function authenticate(username,password){
    return new Promise((resolve,reject)=> {
        const payload = {
            data: {},
            error:null
        };
        setTimeout(() => {
            if(username ==='admin' && password === 'pass'){
                payload.data.authentication_token= 'SECRET';
                payload.data.expire_time = 5000;
                payload.data.role = 'ADMIN';
                resolve(payload.data);
            }else {
                payload.error = 'Invalid credentials';
                reject(payload.error);
            }
        },2000);
    });
}


function redirect(userData,path){
    return new Promise((resolve,reject) => {
        const payload={
            data:{},
            error:null
        };
        if(!userData.role){
            payload.error = 'Unauthorized';
            reject(payload.error);
        } else {
            if(path.includes('admin')){
                payload.data='Hello master';
            } else {
                payload.data='Welcome user';
            }
            resolve(payload.data);
        }
    });
}

//immediately invoc funct execution //iife
//functie anonima
(async function(){
    //o sa am 2 operatii asincrone care depind una de celelalte
    const userData = await authenticate('admin','pass');  //asteapta pana cand auth va intoarce resolve
    const message = await redirect(userData,'/home');
    alert(message);
})();
//am apelat functia odata ce am declarata-> e stearsa de pe stiva de executie
//in window nu am userData
//await se foloseste doar daca am returnat un promise
//tr try catch











