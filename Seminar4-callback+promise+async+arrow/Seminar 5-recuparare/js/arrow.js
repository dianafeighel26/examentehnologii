//cel mai simplu arrow function
//const -  odata initializate, nu mai pot fi modificate
/*
function func1(){
    return 5;
}
*/
//pot scapa de acolade si de return cand am un singur argument
const func1 = () => 5;  
console.log(func1());

/*
function func2(a,b){
    return a+b;
}
*/
const func2 = (a, b) => a+b;
console.log(func2(5,4));

const func3 = a => a+1; //cand am un singur parametru nu mai tr sa pun ()
console.log(func3(3));

const func4 = (a,b) => {
    console.log('Intermediate operation');
    return a+b;
}
console.log(func4(5,4));

//returnare ob dintr un arrow function
const func5 = (a,b) => {
    return {
        a,
        b
    }
}; 
 //cand pun {} creez confuzie si de aceea tr sa pun return 
console.log(func5(5,4));























