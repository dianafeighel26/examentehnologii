//Programarea asincrona
//callback = o functie care se apeleaza in urma unui eveniment
// A callback is a function that is to be executed after another function has finished executing 

//vreau sa simulez procesul de autentificare al unui utilizator intr o pagina
//in urma log in ului vreau sa fac autentificare si dupa sa fac redirect catre home sau catre admin/panel
function authenticate(username,password,callback){
    //declar un obiect ce va avea 2 propr
    const payload = {
        data: {},
        error:null
    };
    setTimeout( () => {
        if(username === 'admin' && password === 'P@ssw0rd'){
            payload.data.authentication_token = 'THIS IS A SECRET';
            payload.data.expire_time = 19999;
            payload.data.role = 'ADMIN';
        } else {
            payload.error = 'Invalid credentials';
        }
        callback(payload.error,payload.data);
    }, 5000);
}


function redirect(userData, path, callback){ //+ calea pe care il redirectionez pe utilizator si un callback
    const payload = {
        data:{},
        error: null
    };
    if(!userData.role){
        payload.error='Unauthorized';
    } else{
        if(path.includes('admin')){
            payload.data = 'HELLO MASTER';
        } else {
            payload.data= 'WELCOME USER';
        }
    }
    callback(payload.error,payload.data);
}

authenticate('admin','P@ssw0rd',(err,data) => {
    if(err)
        throw err;
    const userData = {...data}; // pt a nu face shallow copy //op ... e pt deep copy
    redirect(userData, 'admin/panel', (err,msg) => {
        if(err) throw err;
        alert(msg);
    });
    
 });
//CallStack 
//se adauga in coada de evenimente cu setTimeout
//->contextul global de executie
//->console.log
//->adauga un nouc context pt funnc1
//->stiva va fi goala
//Printr un mecanism numit eventloop ,
//browserul va verifica periodic daca stiva mea de executie este goale
//Atunci cand e goala, ii poate spune cozii ca poate executa functiile care sunt acolo

// setTimeout(() =>{
//     console.log('I am async');
// },0);
// console.log('Do somthing');
// function func1(){
//     console.log('Do something else');
// }

// func1();

