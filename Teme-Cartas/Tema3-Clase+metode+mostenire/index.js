
const FIRST_NAME = "Feighel";
const LAST_NAME = "diana";
const GRUPA = "1077";

/**
 * Make the implementation here
 * Modify Employee and SoftwareEngineer classes from the index.js file to complete the following requirements:
Employee objects should have the following properties: name, surname, salary;
Employee class should contain a method called getDetails that will return a string resulted from the concatentation of the three properties. Example(Name Surname Salary)
SoftwareEngineer class should inherit Employee class;
SoftwareEngineer class should contain a property called experience that will be initialized with value JUNIOR if no value is provided when the object is created.
Create applyBonus method in class SoftwareEngineer that will return the salary with a bonus applied based on experience:
JUNIOR will get a 10% bonus from their salary;
MIDDLE will get a 15% bonus from their salary;
SENIOR will get a 20% bonus from their salary;
If the employee has a different position than the ones mentioned above the bonus will be 10%;
 */
class Employee {
    constructor(name, surname, salary) {
        this.name = name;
        this.surname = surname;
        this.salary = salary;
    }
    getDetails(){
        return this.name+" "+this.surname+" "+this.salary;
    }
}

class SoftwareEngineer extends Employee{
    constructor(name, surname, salary,experience='JUNIOR') {
        super(name, surname, salary);
        this.experience=experience;
    }
    applyBonus(){
        switch(this.experience){
            case 'MIDDLE': 
            return this.salary+this.salary*0.15;
                
            case 'SENIOR': 
            return this.salary+this.salary*0.2;
                
            default: return this.salary+this.salary*0.1;
        }
    }
}

module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    Employee,
    SoftwareEngineer
}

