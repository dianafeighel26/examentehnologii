//instanceOf

function closure(){
    var func = null;
    for(var i=0;i<3;i++){
        func=function(){
            console.log(i);
        };
        console.log(func());
    }
  
    return func;
}

function closure1() {
    var func = null; 
    for(let i=0; i < 3; i++){
        func = function() {
            console.log(i);
        };
        console.log(func());
    }
   
    return func;
}

// console.log(closure());
// console.log(closure1());

function func1(){
    var value = [];   //echivalent cu new Array(); array literals
    for(var i=0;i<3;i++){
        value.push(function(){
            console.log(i);
        });
    }
    return value;
}

function func2(){
    var value = [] ;
    for(let i=0;i<3;i++){
        value.push(function(){
            console.log(i);
        });
    }
    return value;
}
//value e o functie;
//apel de func1 imi returneaza o functie/vector
var result1  = func1();
console.log(result1);

var result2 = func2();
console.log(result2);

result1[0]();
result1[1]();
result1[2]();
result2[0]();
result2[1]();
result2[2]();

//closure=o functie care foloseste
//una sau mai multe variabile dintr-o alta functie

function sum(a,b){
    return a+b;
}

function sum1(a, b){
    return (a||0) + (b||0); //doar in limbajele interpretate
}

function sum2(a,b){
    if(a === undefined) a=0;
    if(b === undefined) b=0;
    return a+b;
}

function sum3(a=0,b=0){
    return a+b;
}

function sum4(a,b)
{
    return (a ? a:0 ) + (b ? b:0); 
}


console.log(sum(5,4));
console.log(sum(5));
//5+undefined = NaN

console.log(sum());
//returneaza undefined + undefined = NaN
console.log(sum1(5,4));
console.log(sum1(5));
console.log(sum1());

console.log(sum2(5,4));
console.log(sum2(5));
console.log(sum2());

console.log(sum3(5,4));
console.log(sum3(5));
console.log(sum3());

console.log(sum4(5,4));
console.log(sum4(5));
console.log(sum4());
