var arr1=new Array();
var arr2=[];

for(let i=0;i<3;i++){
    arr1.push(i+1);
    arr2.push(i+1);
}
console.log(arr1);
console.log(arr2);


//for-uri
// let a = [1,2,3,4]

// for (var j = 0; j < 10; j++){}
// console.log(j)

// console.log('classic for')
// for (let i = 0;i<a.length;i++){
// 	console.log(a[i])
// }

// console.log('for of')
// for (let e of a){
// 	console.log(e)	
// }

// console.log('for in')
// let b = {a : 1, b : 2, c : 3}
// for (let prop in b){
// 	console.log(b[prop])
// }



let a = [1,2,3]
console.log(a[1])
a[3] = 4
console.log(a)
a[8] = 9
console.log(a)
console.log(a.length)
a.push(10)
console.log(a)
console.log(a.pop())
console.log(a)
console.log(a.shift())
console.log(a)
let b = [1,2,3,4]
let c = b.slice(1,3)
console.log(c)
console.log(b.splice(1,2,'a','b','c','d'))
console.log(b)

let d = [1,2,3,4,5]

for (let i = 0; i < d.length; i++) {
  console.log('d[' + i + '] = ' + d[i])
}

for (let e of d){
  console.log(e)
}

console.log(d.indexOf(3))
console.log(d.indexOf('q'))
