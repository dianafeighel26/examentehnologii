function addTokens(input, tokens){
    if(typeof input === 'string'){
        if(input.length <= 6){
            throw {
                message: "Input should have at least 6 characters"
            }
        } else {
            //e bine
            if(typeof tokens === 'object'){
                for(var i=0;i<tokens.length;i++){
                    if(typeof tokens[i].tokenName === 'string'){
                        if(input.includes('...')===true){
                            //string ul e immutabil!!!!!!!!!!!!!!!LA DRACU CU EL!
                               input = input.replace('...',"${"+tokens[i].tokenName+"}");
                            return input;
                        } else {
                            return input;
                        }
                    } else {
                        throw {
                            message: "Invalid array format"
                        }
                    }
                }
            } else {
                throw {
                    message: "Invalid array format"
                }
            }
        }
    } else{
        throw {
            message: "Invalid input"
        }
    }
}

const app = {
    addTokens: addTokens
}

module.exports = app;