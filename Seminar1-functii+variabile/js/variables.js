console.log('My first JS interaction');
console.log(this);
console.log(window);

/*
    Data types:
    -number
    -string
    -boolean
    -null
    -undefined
    -Symbol
    -BigInt
*/

var a=10;
console.log(typeof a);
console.log(a);

a="Hello there";
console.log(typeof a);
console.log(a);

a=false;
console.log(typeof a);
console.log(a);

a=null;
console.log(typeof a);
console.log(a);

{
    let b='Block scoped';
    console.log(b);
}

//let->vizibila doar in {} in care a fost declarata
//var->vizibila in toata functia in care a fost declarata

const c='I am immutable';
console.log(c);
//c='reinitialize';
//console.log(b);





// a=Symbol('unique');
// console.log(a);
// console.log(typeof(a));

// a=BigInt(10);
// console.log(a);
// console.log(typeof(a));

// var a=10;
// var b;

// console.log(a);
// console.log(b);  //typeof(b) = undefined, dar avem alocata memorie, desi nu e initializata
// console.log(C);
// //window, obiectul this



var bb= undefined; //never do this
var cc=null;
console.log(bb);  
console.log(cc);
console.log(bb==cc);  //value check ->true

console.log(bb===cc); //value check + type check   ->false
