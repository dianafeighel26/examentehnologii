import React, { Component } from 'react'

// TODO : adăugați posibilitatea de a edita un robot 
// editarea se face prin intermediul unui robot cu 2 stări, una de vizualizare și una de editare

// TODO : add the posibility to edit a robot 
// editing is done via 2 states a view state and an edit state

class Robot extends Component {
	constructor(props){
		super(props)
		this.state={
			edit :false,
				type : '',
			name : '',
			mass : 0,
			typeEdit:'',
			nameEdit:'',
			massEdit:''
		}
	}
	
	handleonclick=()=>{
		this.setState({
			edit : true
		})
	}
		onhandleClickButtonSave=()=>{
		var obj={};
		obj.name=this.state.nameEdit;
		obj.type=this.state.typeEdit;
		obj.mass=this.state.massEdit;
		obj.id=this.props.item.id
		 this.props.onSave(this.props.item.id,obj);
	}
	handleonclickCancel=()=>{
			this.setState({
			edit : false
		})
	}
	   handleOnChangeName = (event) => {
        this.setState({
            nameEdit: event.target.value
        });
    }
     handleOnChangeType= (event) => {
        this.setState({
            typeEdit: event.target.value
        });
    }
     handleOnChangeMass = (event) => {
        this.setState({
            massEdit: event.target.value
        });
    }
	render() {
		let { item } = this.props
		if (this.state.edit === false) {
			return (
				<div>
				Hello, my name is {item.name}. I am a {item.type} and weigh {item.mass}
				<button type="button" value="edit" onClick={this.handleonclick}/>
				
			</div>
			)
		}
		else {
			return (
				<div>
				<button type="button" value="cancel" onClick={this.handleonclickCancel}/>
				Hello, my name is {item.name}. I am a {item.type} and weigh {item.mass}
				<button type="button" value="edit" onClick={this.handleonclick}/>
  
					<button type="button" value="save" onClick={this.onhandleClickButtonSave}/>
					<input type="text" id="name" name="name"  value = {this.state.name} onChange = {this.handleOnChangeName} />
		         Type:
		         <input type="text" id="type" name="type"  value = {this.state.type} onChange = {this.handleOnChangeType} />
		         Mass:
		         <input type="text" id="mass" name="mass"  value = {this.state.mass} onChange = {this.handleOnChangeMass} />
		      	</div>
			)
			}

			}
		}

export default Robot
