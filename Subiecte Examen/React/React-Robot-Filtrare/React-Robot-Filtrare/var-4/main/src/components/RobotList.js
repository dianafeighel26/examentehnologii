import React, { Component } from 'react'
import RobotStore from '../stores/RobotStore'
import Robot from './Robot'

// TODO: adăugați posibilitatea de a filtra roboții după name și type
// filtrarea se face pe baza a 2 text input-uri și se produce la fiecare tastă apăsată

// TODO: add the possiblity to filter robots according to name and type
// filtering is done via 2 text inputs and happens whenever a key is pressed

class RobotList extends Component {
	constructor(){
		super()
		this.state = {
			robots : [],
			auxRobotsName:[],
			auxRobotsType:[]
			
		}
	
	}
	componentDidMount(){
		this.store = new RobotStore()
		this.setState({
			robots : this.store.getRobots(),
			auxRobotsName : this.store.getRobots(),
			auxRobotsType : this.store.getRobots()
		})
		this.store.emitter.addListener('UPDATE', () => {
			this.setState({
				robots : this.store.getRobots(),
				auxRobotsName : this.store.getRobots(),
				auxRobotsType : this.store.getRobots()
			})			
		})
	
	}
	
	handleOnChangeName=(event)=>{
		var listaNouaName=[];
		for(var i=0;i<this.state.robots.length;i++){
			if(this.state.robots[i].name.includes(event.target.value)===true){
				listaNouaName.push(this.state.robots[i]);
			}
		}
		this.setState({
			auxRobotsName: listaNouaName
		})
	}
	handleOnChangeType=(event)=>{
		var listaNouaType=[];
		for(var i=0;i<this.state.robots.length;i++){
			if(this.state.robots[i].type.includes(event.target.value)===true){
				listaNouaType.push(this.state.robots[i]);
			}
		}
			this.setState({
			auxRobotsType: listaNouaType
		})
	}
	render() {
			//verificare pe ambele fitre
		var auxRobotsBoth = [];
		for(var i=0;i<this.state.auxRobotsType.length;i++){
			for(var j=0;j<this.state.auxRobotsName.length;j++){
				if(this.state.auxRobotsName[i] === this.state.auxRobotsType[j]){
				auxRobotsBoth.push(this.state.auxRobotsType[i]);
				}
			}
		}
		return (
			<div>
				{
					auxRobotsBoth.map((e, i) => 
						<Robot item={e} key={i} />
					)
				}
				<input type="text" id="nameFilter" onChange = {this.handleOnChangeName}/>
				<input type="text" id="typeFilter" onChange = {this.handleOnChangeType}/>
			</div>
		)
	}
}

export default RobotList
