import React from 'react';
import { AddCar} from './AddCar';

export class CarList extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            cars: []
        };
    }   

    onAddMethod = (car) => {
        // const obj = Object.assign({}, this.state);
        const newCars = this.state.cars;
        newCars.push(car);
        this.setState({
            cars: newCars
        });
    }
    render(){
          let items = this.state.cars.map((car, index) => <div key={index}>{car.make}</div>);
        return (
            <div>
            <AddCar onAdd={this.onAddMethod}/>
            <div>{items}</div>
            </div>
        )
    }
}