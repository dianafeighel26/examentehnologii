import React from 'react';



class Robot extends React.Component{
    
    handleClickButton = (event)=>{
        //din compon robot  apelez props ul din robotlist
        this.props.onDelete(this.props.item.id);
    }
    
    render(){
        	//pt : // o componentă robot ar trebui să afișeze un robot și să permită ștergerea lui
		//=>trimit un obiect din lista un robot prin props 
		//in cazul in care trebuie sa afisam ceva!!!->celelalte doua teste sunt pt stergere
		//pt stergere in test am un button
		const {item} = this.props;
        return (
            <div>
                
                Hi {item.name}
                <input type="button" value="delete" onClick={this.handleClickButton}/>
            </div>
            );
    }
}

export default Robot;