var LIB1 = window.LIB1 || {};

//in cazul in care alticenva s a gandit la acelasi nume de variabila, pot sa zic:
// const LIB1
//sau window.LIB1 || {}
//IIFE
(function(){
    LIB1.a= 'something';
    LIB1.getDetails=function(name, surname){
        return `${name} ${surname}`;
    }
    //vreau sa apelez getDetails nu secvential; ci intr un handler
    document.addEventListener('DOMContentLoaded', (event) => {
        console.log(LIB1.getDetails('Gigel','Popel'));
        console.log(LIB2.getDetails('Iphone',5000));
    });
}())

//atunci cand declar functia pe care o execut, totate var din functie nu mai apartin de window, ci de functia mea anonima
//problema e ca eu am codul incapsulat in functii  
//=> ajungem la conceptul de namespace->in javascript este notiunea de modul
//pot sa imi separ librariile mele cu ajutorul unor obiecte  -> creez ob gol LIB1 si ma folosesc de el 

//Block scope variables -> pot simula acelasi lucru
{

}

