//Manipulare Dom
//1.fie lucrez cu elementele
//2.fie am o serie de identificatori prin care pot sa gasesc elem pe care il caut

//Vreau sa accesez butonul meu
// const button = document.querySelector('#my-btn');
// console.log(button);
// button.addEventListener('click', (event) => {
//     console.log(event.currentTarget);  //referinta
// });

//Cand se interpreteaza documentul html:
//cand se intalneste tag ul de script, randarea are un STOP; se face switch ul la dom.js
//Cea mai simpla abordare e sa pun tag ul de script dupa declararea butonului.
//in paralel se randeaza si arborele pt CSS
//Atunci cand incarcam fisierul js tr sa avem grija sa nu facem direct manipulare de dom. !!


window.addEventListener('load', (event) => {
    console.log('All assets loaded');
    const button = document.querySelector('#my-btn');
console.log(button);
button.addEventListener('click', (event) => {
    console.log(event.currentTarget);
});
});
 //exista posibilitatea sa nu fie asa rapid load

 document.addEventListener('DOMContentLoaded',(event) => {
    console.log('Dom is loaded');
    const button = document.querySelector('#my-btn');
console.log(button);
button.addEventListener('click', (event) => {
    console.log(event.currentTarget);
});
});
//asa e mai rapid

